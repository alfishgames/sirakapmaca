﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.Dynamics;

public class Player : MonoBehaviour
{

    private Transform sidePos;

    private float firstPosX;
    private Rigidbody rb;
    private bool isMoving;
    private bool isSide;
    private Animator myAnim;

    private GameObject winScreen;
    
    void Start()
    {
        sidePos = GameObject.Find("TargetPos").transform;
        winScreen = GameObject.Find("MenuUI").transform.GetChild(0).gameObject;
        myAnim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        firstPosX = transform.position.x;
    }


    void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            myAnim.SetBool("Run", true);
            isMoving = true;
            transform.position = Vector3.Lerp(transform.position, new Vector3(sidePos.position.x, transform.position.y, transform.position.z), 5 * Time.deltaTime);
            if (isSide)
            {
                transform.Translate(Vector3.forward * 3 * Time.deltaTime);
            }
        }

        else
        {
            myAnim.SetBool("Run", false);
            isMoving = false;
            transform.position = Vector3.Lerp(transform.position, new Vector3(firstPosX, transform.position.y, transform.position.z), 20 * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "AI")
        {
            if (!isMoving)
            {                
                myAnim.SetTrigger("Hit");

                PuppetMaster aiPuppet = other.gameObject.transform.parent.GetComponentInChildren<PuppetMaster>();
                //aiPuppet.state = PuppetMaster.State.Dead;

                other.GetComponent<AITarget>().GetHandle();

                Animator anim = other.gameObject.GetComponent<Animator>();
                anim.SetTrigger("FallLeft");
            }
        }
        if (other.tag == "Side")
        {
            isSide = true;
        }

        if (other.gameObject.layer == 10)
        {
            winScreen.SetActive(true);


        }


    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Side")
        {
            isSide = false;
        }
    }
}