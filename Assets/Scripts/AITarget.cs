﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITarget : MonoBehaviour
{
    private bool rootAnim;
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (rootAnim)
        {
            transform.parent.gameObject.transform.position = Vector3.Lerp(transform.parent.transform.position, new Vector3(-2.92f, transform.position.y, transform.position.z), 20 * Time.deltaTime);
            transform.LookAt(FindObjectOfType<Player>().gameObject.transform);
        }
    }

    public void GetHandle()
    {
        StartCoroutine(FallAnimFinish());
    }

    IEnumerator FallAnimFinish()
    {
        yield return new WaitForSeconds(2.28f);
        rootAnim = true;
    }
}
