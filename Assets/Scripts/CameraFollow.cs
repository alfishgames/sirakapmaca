﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothspeed;
    public Vector3 offset;

    Vector3 supportvec;

    void FixedUpdate()
    {
        supportvec = new Vector3(target.position.x, target.position.y, 0);
        Vector3 desiredPosition = (target.position - supportvec) + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothspeed * Time.deltaTime);
        transform.position = smoothedPosition;
    }
}
