﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIHitManager : MonoBehaviour
{

    bool yourWayIsReverse;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        if (AiManager.instance.whatsYourPosition == 0)
        {
            yourWayIsReverse = false;
        }

        if (AiManager.instance.movingList.Length - 1 == AiManager.instance.whatsYourPosition)
        {
            yourWayIsReverse = true;
        }



    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.name == "T-Pose")
        {
            
            if (!yourWayIsReverse) {  
            AiManager.instance.whatsYourPosition += 1;               
            }
            else 
            {
                AiManager.instance.whatsYourPosition -= 1;
            }
        }

    }



}
