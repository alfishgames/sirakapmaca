﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiManager : MonoBehaviour
{

    public Transform[] movingList;

    // Transforms to act as start and end markers for the journey.
    public Transform startMarker;
    public Transform endMarker;

    Animator anim;

    // Movement speed in units per second.
    public float speed;

    // Time when the movement started.
    private float startTime;

    // Total distance between the markers.
    private float journeyLength;

    public static AiManager instance;

   public int whatsYourPosition = 0;

    private bool moveCharacter = false;

    private void Awake()
    {
        if (instance == null) instance = this;
    }



    void Start()
    {
        // Keep a note of the time the movement started.
        startTime = Time.time;

        anim = GetComponent<Animator>();

        // Calculate the journey length.
        startMarker = transform;
        endMarker = movingList[whatsYourPosition];


        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);


    }

    void FixedUpdate()
    {

        endMarker = movingList[whatsYourPosition];
        //// Distance moved equals elapsed time times speed..

        //float distCovered = (Time.time - startTime) * speed;


        ////   Fraction of journey completed equals current distance divided by total distance.

        //float fractionOfJourney = distCovered / journeyLength;


        // Set our position as a fraction of the distance between the markers.
        
     

        RotateCharacter();


        if (moveCharacter == true)
        {
            MoveCharacter();
        }



    }


    public void RotateCharacter()
    {
        // transform.LookAt(endMarker);

        Vector3 lTargetDir = endMarker.position - transform.position;
        lTargetDir.y = 0.0f;
        transform.rotation = Quaternion.RotateTowards(transform.rotation,
        Quaternion.LookRotation(lTargetDir), Time.deltaTime * 85f);


        RaycastHit hit;
        int layerMask = LayerMask.GetMask("AIObject");
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);

            if (hit.collider.gameObject.name == whatsYourPosition.ToString())
            {
                moveCharacter = true;
                anim.SetBool("run", true);

            } else
            {
                moveCharacter = false;
                anim.SetBool("run", false);

            }


        }

    }


    public void MoveCharacter()
    {
        transform.position = Vector3.Lerp(startMarker.position, endMarker.position, 0.01f);

    }



}



